package org.tbasket.endpoint.request


case class RegisterRequest(name: String, forename: String, email: String, password: String)

case class LoginRequest(email: String, password: String)