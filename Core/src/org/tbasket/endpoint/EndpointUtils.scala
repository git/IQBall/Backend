package org.tbasket.endpoint

import io.netty.handler.codec.http.QueryStringDecoder
import org.tbasket.error.*
import zio.http.Body
import zio.http.api.openapi.OpenAPI.Parameter.QueryStyle.Form
import zio.json.*
import zio.json.ast.{Json, JsonCursor}
import zio.{Task, ZIO, http}

import java.net.URLDecoder
import java.nio.charset.StandardCharsets
import scala.deriving.Mirror
import scala.jdk.CollectionConverters.*
import scala.language.{implicitConversions, reflectiveCalls}

object EndpointUtils {

  
  def errorBody(errorType: String, msg: String) = Body.fromString(s"""{"error": "$errorType","msg": "$msg"}""")
  
  inline def decode[A <: Product](body: Body)(using mirror: Mirror.Of[A]) =
    for
      str <- body.asString
      decoded <- ZIO.fromEither(str.fromJson(DeriveJsonDecoder.gen[A]))
        .mapError(InvalidRequest("Invalid request body", _))
    yield decoded
  
}