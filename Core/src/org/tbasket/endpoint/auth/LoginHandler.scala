package org.tbasket.endpoint.auth

import io.getquill.*
import io.getquill.context.ZioJdbc.*
import io.getquill.context.qzio.{ZioContext, ZioJdbcContext}
import io.getquill.context.sql.idiom.SqlIdiom
import org.apache.logging.log4j.{LogManager, Logger}
import org.tbasket.auth.Authenticator
import org.tbasket.data.{DatabaseContext, User}
import org.tbasket.endpoint.EndpointUtils.{decode, errorBody}
import org.tbasket.endpoint.PageHandler
import org.tbasket.endpoint.request.LoginRequest
import org.tbasket.error.*
import zio.*
import zio.http.*
import zio.http.model.{Cookie, Header, Headers, Status}
import zio.json.*
import zio.json.ast.Json.Str
import zio.json.ast.{Json, JsonCursor}

import java.sql.SQLException
import java.util.UUID


object LoginHandler extends PageHandler:
  
  implicit private final val Log: Logger = LogManager.getLogger("/login")
  
  private def tryLogin(request: Request) =
    for
      request <- decode[LoginRequest](request.body)
      user <- ZIO.serviceWithZIO[Authenticator](_.loginUser(request.email, request.password))
      
      jwt <- ZIO.serviceWithZIO[Authenticator](_.requestNewJwt(user))
    yield Response(
      status = Status.Found,
      headers = Headers.location("/") ++ //login successful, go back to main page
        Headers.setCookie(Cookie("JWT", jwt)) //and set the token cookie
    )
  
  def login(request: Request) =
    tryLogin(request).catchSome {
      case UserNotFound(msg) => ZIO.attempt(Response(
        status = Status.Found,
        body = errorBody("unauthorized", msg),
        headers =
          Headers(
            Headers.location("/register")
          ) // send back caller to register panel
      ))
      
      case InvalidPassword(msg) => ZIO.attempt(Response(
        status = Status.Unauthorized,
        body = errorBody("unauthorized", msg)
      ))
    }

