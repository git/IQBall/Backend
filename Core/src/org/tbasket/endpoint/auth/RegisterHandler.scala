package org.tbasket.endpoint.auth

import org.apache.logging.log4j.{LogManager, Logger}
import org.tbasket.auth.Authenticator
import org.tbasket.data.User
import org.tbasket.endpoint.EndpointUtils.*
import org.tbasket.endpoint.PageHandler
import org.tbasket.endpoint.request.RegisterRequest
import org.tbasket.error.*
import zio.ZIO
import zio.http.model.{Cookie, Headers, Status}
import zio.http.{Body, Request, Response, model}
import zio.json.*
import zio.json.ast.{Json, JsonCursor}

object RegisterHandler extends PageHandler {
  
  
  private def tryRegister(request: Request) =
    for
      request <- decode[RegisterRequest](request.body)
      
      user <- ZIO.serviceWithZIO[Authenticator](_.registerUser(request.name, request.forename, request.email, request.password))
      jwt <- ZIO.serviceWithZIO[Authenticator](_.requestNewJwt(user))
    yield Response(
      status = Status.Found,
      headers = Headers.location("/") ++ //register successful, go back to main page
        Headers.setCookie(Cookie("JWT", jwt))
    )
  
  def register(request: Request) = tryRegister(request)
    .catchSome {
      case InvalidEmail(msg)          => ZIO.attempt(Response(
        status = Status.ExpectationFailed,
        body = errorBody("invalid email", msg)
      ))
      case InvalidPassword(msg) => ZIO.attempt(Response(
        status = Status.ExpectationFailed,
        body = errorBody("invalid password", msg)
      ))
      case InvalidName(msg)       => ZIO.attempt(Response(
        status = Status.ExpectationFailed,
        body = errorBody("invalid name", msg)
      ))
      case InvalidForename(msg)       => ZIO.attempt(Response(
        status = Status.ExpectationFailed,
        body = errorBody("invalid forename", msg)
      ))
      case UserAlreadyRegistered(msg) =>
        ZIO.attempt(Response(
          status = Status.NotAcceptable,
          body = errorBody("already registered", msg),
          headers = Headers.location("/login") //the account already exists so we move the user to login page
        ))
    }
  
}
