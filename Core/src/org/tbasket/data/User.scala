package org.tbasket.data

import io.getquill.*

import java.util.UUID

final case class User(
                 id          : UUID,
                 name        : String,
                 forename    : String,
                 passwordHash: Int,
                 mailAddress : String
               )

