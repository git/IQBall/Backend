package org.tbasket.data

import io.getquill.*
import io.getquill.context.ZioJdbc.{DataSourceLayer, QuillZioExt}
import io.getquill.context.qzio.ZioContext
import io.getquill.idiom.Idiom
import io.getquill.jdbczio.Quill
import org.apache.logging.log4j.LogManager
import org.sqlite.{SQLiteDataSource, SQLiteException}
import org.tbasket.config.ServerConfig
import org.tbasket.data.Database.LOG
import zio.*

import java.io.Closeable
import java.sql.SQLException
import java.util.Properties
import javax.sql

//TODO this class is a veritable fraud
class Database(config: ServerConfig):
  
  private var initialized = false
  
  val contextLayer    = ZLayer.succeed(DatabaseContext(new SqliteZioJdbcContext(SnakeCase)))
  val datasourceLayer = Quill.DataSource.fromPrefix(config.databaseConfigName).tap { ds =>
    if (initialized) ZIO.succeed(ds)
    else ZIO.attempt {
      initialized = true
      val requests = new String(getClass.getResourceAsStream("/table_init.sql").readAllBytes()).split(';')
      val stmnt    = ds.get.getConnection
        .createStatement()
      requests.foreach { sql =>
        try {
          stmnt.execute(sql)
        } catch {
          case e: SQLException if e.getMessage.contains("already exists") =>
            //do nothing
        }
      }
      stmnt.close()
    }
  }

object Database {
  val LOG = LogManager.getLogger("Database")
}