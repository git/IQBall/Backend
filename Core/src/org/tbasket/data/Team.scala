package org.tbasket.data

import io.getquill.*

case class Team(id: Int, name: String, clubName: String)
