package org.tbasket.data

import io.getquill.*

case class Tactic(id: Int, name: String, owner: User, filePath: String)

