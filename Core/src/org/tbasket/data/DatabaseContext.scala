package org.tbasket.data

import io.getquill.NamingStrategy
import io.getquill.context.qzio.ZioJdbcContext
import io.getquill.context.sql.idiom.SqlIdiom

case class DatabaseContext(v: ZioJdbcContext[SqlIdiom, NamingStrategy]):
  export v.*



