package org.tbasket.data

import io.getquill.*

case class Member(team: Team, user: User, admin: Boolean)