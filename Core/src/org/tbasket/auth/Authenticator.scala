package org.tbasket.auth

import io.circe.*
import io.circe.generic.auto.*
import io.circe.parser.*
import io.circe.syntax.*
import io.getquill.*
import io.getquill.context.qzio.ZioJdbcContext
import io.getquill.context.sql.idiom.SqlIdiom
import org.apache.logging.log4j.LogManager
import org.tbasket.auth.Authenticator.*
import org.tbasket.data.{DatabaseContext, User}
import org.tbasket.error.*
import pdi.jwt.algorithms.JwtAsymmetricAlgorithm
import pdi.jwt.{JwtClaim, JwtZIOJson}
import zio.*
import zio.http.*
import zio.http.api.HttpCodec.Method
import zio.http.model.Status.{InternalServerError, Ok}
import zio.http.model.Version.Http_1_1
import zio.http.model.{Headers, Method}

import java.io.ByteArrayInputStream
import java.security.PublicKey
import java.util.UUID
import javax.sql.DataSource
import scala.collection.immutable.HashMap


object Authenticator:
  private final val LOG                  = LogManager.getLogger("Authentification")
  private final val ValidMailPattern     = "^\\w+([.-]?\\w+)*@\\w+([.-]?\\w+)*(\\.\\w{2,3})+$".r
  private final val ValidForenamePattern = "^[a-zA-Z\\u00C0-\\u017F-']+$".r
  private final val ValidNamePattern            = ValidForenamePattern
  private final val ValidPasswordPattern = ".{6,}".r
  
  case class JwtContent(uuid: UUID)


class Authenticator(url: URL, key: PublicKey, algorithm: JwtAsymmetricAlgorithm) {
  
  
  private def defineCustomClaims(user: User): String = {
    JwtContent(user.id).asJson.noSpaces
  }
  
  private def mkRequest(user: User): Request = {
    val custom = defineCustomClaims(user)
    Request(Body.fromString(custom), Headers.empty, Method.GET, url, Http_1_1, None)
  }
  
  def requestNewJwt(user: User) = {
    Client.request(mkRequest(user))
      .flatMap {
        case Response(Ok, _, body, _, _)                  =>
          body.asString
        case Response(InternalServerError, _, body, _, _) =>
          ZIO.fail(EmitterInternalError(_)).flatMap(f => body.asString.map(f))
        case r                                            =>
          ZIO.fail(UnrecognizedEmitterResponse(s"Received unknown response from emitter ${r}"))
      }
  }
  
  private def hashPassword(password: String) = {
    password.hashCode //TODO user BCrypt or argon2id
  }
  
  private def findByMail(mail: String) = ZIO.serviceWithZIO[DatabaseContext] { ctx =>
    import ctx.v.*
    run(query[User].filter(_.mailAddress == lift(mail))).map(_.headOption)
  }
  
  def loginUser(mail: String, password: String) = ZIO.serviceWithZIO[DatabaseContext] { ctx =>
    import ctx.v.*
    
    findByMail(mail)
      .someOrFail(UserNotFound(s"user not found for email address $mail"))
      //            await one second if password fails to reduce bruteforce //FIXME this wont actually reduce bruteforce
      .filterOrElse(_.passwordHash == hashPassword(password))(Clock.sleep(1.seconds) *> ZIO.fail(InvalidPassword("invalid password")))
  }
  
  
  def registerUser(name: String, forename: String, mail: String, password: String) = ZIO.serviceWithZIO[DatabaseContext] { ctx =>
    import ctx.*
    lazy val user = {
      val uuid = UUID.randomUUID()
      val hash = hashPassword(password)
      User(uuid, name, forename, hash, mail)
    }
    
    if (!ValidMailPattern.matches(mail))
      ZIO.fail(InvalidEmail(s"email address did not satisfy regex pattern"))
    else if (!ValidPasswordPattern.matches(password))
      ZIO.fail(InvalidPassword(s"password did not satisfy regex pattern"))
    else if (!ValidForenamePattern.matches(forename))
      ZIO.fail(InvalidForename(s"email address did not satisfy regex pattern"))
    else if (!ValidNamePattern.matches(name))
      ZIO.fail(InvalidName(s"password did not satisfy regex pattern"))
    else for
      _ <- findByMail(mail).none.orElse(ZIO.fail(UserAlreadyRegistered(s"an email address already exists for '$mail'")))
      _ <- run(quote {
        query[User].insert(
          _.id -> lift(user).id,
          _.name -> lift(user).name,
          _.forename -> lift(user).forename,
          _.passwordHash -> lift(user).passwordHash,
          _.mailAddress -> lift(user).mailAddress,
        )
      }).fork
    yield user
  }
  
  
  def validateAndGetUser(jwt: String) = {
    for
    //decoding token
      claims <-
        ZIO.fromTry(JwtZIOJson.decode(jwt, key, Seq(algorithm)))
          .mapError(InternalError.apply)
      
      //ensure that the token is not expired (or else fail)
      _ <- ZIO.attempt(claims.expiration)
        .someOrElseZIO(ZIO.dieMessage("Invalid token"))
        .filterOrFail(_ <= java.lang.System.currentTimeMillis())(ExpiredJwt)
      
      //
      uuid <- ZIO.attempt(claims.content)
        .mapAttempt(decode[JwtContent](_))
        .flatMap(ZIO.fromEither(_))
        .map(_.uuid)
        .mapError(InternalError.apply)
      
      user <- ZIO.serviceWithZIO[ZioJdbcContext[SqlIdiom, NamingStrategy]] { ctx =>
        import ctx.*
        run(quote {
          query[User].filter(_.id == lift(uuid))
        }).map(_.headOption)
          .someOrFail("uuid not found.")
      }
    yield user
  }
}
