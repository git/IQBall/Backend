package org.tbasket

import org.apache.logging.log4j.LogManager
import org.tbasket.auth.Authenticator
import org.tbasket.config.{FileServerConfig, ServerConfig}
import org.tbasket.data.Database
import org.tbasket.endpoint.Endpoint
import pdi.jwt.algorithms.JwtAsymmetricAlgorithm
import zio.*
import zio.http.URL

import java.lang
import java.nio.file.{Files, Path}
import java.security.PublicKey
import java.security.cert.CertificateFactory
import java.security.spec.{KeySpec, PKCS8EncodedKeySpec, RSAPrivateKeySpec, X509EncodedKeySpec}
import java.util.Properties
import scala.io.StdIn
import scala.util.control.NonFatal
import scala.util.{Failure, Success}

object Main extends ZIOAppDefault:
  final val LOG = LogManager.getLogger("Core")
  
  override def run = ZIO.serviceWithZIO[ZIOAppArgs] { args =>
    retrieveConfig(args)
      .flatMap(config => setupAuth(config) <&> setupDatabase(config) <&> setupEndpoint(config))
      .flatMap {
        case (auth, db, ep) =>
          ep.run.provide(db.datasourceLayer, db.contextLayer, auth)
      }
  }
  
  private def setupEndpoint(config: ServerConfig) = ZIO.attempt {
    new Endpoint(config.endpointPort)
  }
  
  private def setupDatabase(config: ServerConfig) = ZIO.attempt {
    new Database(config)
  }
  
  private def setupAuth(config: ServerConfig) = ZIO.attempt {
    val publicKey = config.emitterCertificate.getPublicKey
    val auth      = new Authenticator(config.emitterURL, publicKey, config.emitterKeyAlgorithm)
    ZLayer.succeed(auth)
  }
  
  
  private def retrieveConfig(args: ZIOAppArgs): Task[ServerConfig] = ZIO.attempt {
    val configFile = Path.of("server.properties")
    if Files.notExists(configFile) then
      val in = getClass.getResourceAsStream("/server.properties")
      Files.writeString(configFile, new String(in.readAllBytes()))
    
    val in         = Files.newInputStream(configFile)
    val properties = new Properties()
    properties.load(in)
    properties
  }.flatMap(p => FileServerConfig(p, args.getArgs))
  
  // add a shutdown hook to log when the server is about to get killed
  lang.Runtime.getRuntime.addShutdownHook(new Thread(() =>
    LOG.info("Server shutdowns")
  ))
