package org.tbasket.error

import org.apache.logging.log4j.{LogManager, Logger}
import zio.http.Response
import zio.http.model.Status
import zio.{Task, ZIO}

sealed class RegularException(msg: String, cause: Throwable = null) extends Exception(msg, cause)

case class InternalError(cause: Throwable) extends Exception(cause)

case class InvalidArgumentError(cause: String) extends Exception(cause)


case class InvalidRequest(msg: String, cause: String = "") extends Exception(msg + (if cause.nonEmpty then ":" + cause else "")) with UserException


