package org.tbasket.error

sealed class JwtException(msg: String) extends Exception(msg)

case class InvalidJwt(msg: String) extends JwtException(msg) with UserException

case class ExpiredJwt(msg: String) extends JwtException(msg) with UserException


case class UnrecognizedEmitterResponse(msg: String) extends JwtException(msg)

case class EmitterInternalError(msg: String) extends JwtException(msg)
