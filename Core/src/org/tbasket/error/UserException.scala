package org.tbasket.error

/**
 * User exceptions are a special type of exception where stack trace is not filled as those exceptions are not meant to be printed in the console
 * because they are predictable error.
 * 
 * */
trait UserException extends Exception {
  override def fillInStackTrace(): Throwable = this //do not fill stack trace
}
