package org.tbasket.error

sealed class AuthException(msg: String) extends Exception(msg) with UserException

case class InvalidPassword(msg: String) extends AuthException(msg)

case class InvalidEmail(msg: String) extends AuthException(msg)

case class UserNotFound(msg: String) extends AuthException(msg)

case class UserAlreadyRegistered(msg: String) extends AuthException(msg)


case class InvalidName(msg: String) extends AuthException(msg)

case class InvalidForename(msg: String) extends AuthException(msg)

