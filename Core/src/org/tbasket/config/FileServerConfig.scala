package org.tbasket.config

import org.tbasket.ServerConfigException
import org.tbasket.config.FileServerConfig.CertFactory
import pdi.jwt.JwtAlgorithm
import pdi.jwt.algorithms.JwtAsymmetricAlgorithm
import zio.http.URL
import zio.stream.ZStream
import zio.{Chunk, Task, ZIO, ZIOAppArgs}

import java.nio.file.{Files, Path}
import java.security.cert.{Certificate, CertificateFactory}
import java.util.Properties

final class FileServerConfig private(userProperties: Properties, schema: Properties, arguments: Map[String, String]) extends ServerConfig {
  
  private def getPropertySafe(name: String) =
    if (schema.getProperty(name) == null) {
      throw new ServerConfigException(
        s"""
           |  current config seems expired : property $name should not be present in your config
           |  (maybe this server has a newer version than the configuration and its was modified)
           |  config schema :
           |
           |$schemaString
           |
           | You may try to adapt your old config version with new schema
           |""".stripMargin)
    }
    Option(userProperties.getProperty(name)) match
      case Some(value) => value
      case None        =>
        arguments.getOrElse(name, throw new ServerConfigException(
          s"""
             |  could not find property in server configuration : $name
             |  config schema :
             |
             |$schemaString
             |
             |  This property is required.
             |""".stripMargin))
  
  
  
  override val emitterURL: URL = URL.fromString(getPropertySafe("emitter.url")) match
    case Left(exception) => throw exception
    case Right(value)    => value
  
  override val emitterCertificate: Certificate = {
    val path = Path.of(getPropertySafe("emitter.cert"))
    val in   = Files.newInputStream(path)
    CertFactory.generateCertificate(in)
  }
  
  override val emitterKeyAlgorithm = JwtAlgorithm.RS256
  
  override val endpointPort: Int =
    getPropertySafe("endpoint.port")
      .toIntOption
      .getOrElse(throw new ServerConfigException("endpoint.port is not an integer"))
  
  
  override val databaseConfigName: String = getPropertySafe("database.prefix")
  
  
  override def pagesLocation: Option[Path] = Some(Path.of(getPropertySafe("pages.location")))
  
  private def schemaString = {
    schema.stringPropertyNames()
      .toArray(new Array[String](_))
      .map(key => s"$key=${schema.getProperty(key)}")
      .mkString("\n")
  }
  
}

object FileServerConfig {
  //TODO make certificate type configurable
  final val CertFactory = CertificateFactory.getInstance("X509")
  
  def apply(userProperties: Properties, arguments: Seq[String]): Task[ServerConfig] = ZIO.attempt {
    val args     = arguments.map {
      case s"$key=$value" => (key, value)
      case x              => throw new ServerConfigException(s"configuration parameter must be of format 'property.name=value' received : ${x}")
    }.toMap
    val schemaIn = getClass.getClassLoader.getResourceAsStream("server.properties")
    val schema   = new Properties()
    schema.load(schemaIn)
    new FileServerConfig(userProperties, schema, args)
  }
  
}
