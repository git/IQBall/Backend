package org.tbasket.config

import pdi.jwt.JwtAlgorithm
import pdi.jwt.algorithms.JwtAsymmetricAlgorithm
import zio.http.URL

import java.nio.file.Path
import java.security.cert.Certificate

trait ServerConfig {
  def emitterURL: URL
  
  def emitterCertificate: Certificate
  
  def emitterKeyAlgorithm: JwtAsymmetricAlgorithm
  
  def endpointPort: Int
  
  def databaseConfigName: String
  
  def pagesLocation: Option[Path]
  
}
