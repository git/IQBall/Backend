package org.tbasket.test

import org.tbasket.auth.Authenticator
import org.tbasket.config.ServerConfig
import org.tbasket.data.Database
import zio.{Task, ZLayer}

import java.nio.file.{Files, Path}
import java.sql.{DriverManager, Statement}

/*
* Defines required test service layers
* */
object TestLayers {
  
  
  val auth = {
    TestEmitter.start()
    val publicKey = TestServerConfig.emitterCertificate.getPublicKey
    val auth      = new Authenticator(TestServerConfig.emitterURL, publicKey, TestServerConfig.emitterKeyAlgorithm)
    ZLayer.succeed(auth)
  }
  
  val db = {
    //ensure that the test table is always new in order to make tests on the same dataset all the time.
    Files.deleteIfExists(Path.of("/tmp/test-database.sqlite"))
    
    //open database
    val db = new Database(TestServerConfig)
    
    //fill the test database with a test dataset
    val connection = DriverManager.getConnection("jdbc:sqlite:/tmp/test-database.sqlite")
    val stmnt      = connection.createStatement()
    executeFile(stmnt, "table_init.sql")
    executeFile(stmnt, "test_dataset.sql")
    stmnt.close()
    connection.close()
    
    db
  }
  
  private def executeFile(statement: Statement, url: String) =
    val in    = getClass.getClassLoader.getResourceAsStream(url)
    val bytes = in.readAllBytes()
    in.close()
    
    val requests = new String(bytes).split(';')
    requests.foreach(statement.execute)
}
