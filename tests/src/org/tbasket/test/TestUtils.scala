package org.tbasket.test

import io.netty.handler.codec.http.QueryStringEncoder
import org.tbasket.error.InvalidRequest
import zio.*
import zio.http.{Body, Response}
import zio.json.*
import zio.json.ast.{Json, JsonCursor}

import scala.language.{implicitConversions, reflectiveCalls}

object TestUtils {
  
  def parseAttribute[V, T <: Json {def value: V}](json: Json, cursor: JsonCursor[Json, T]): Task[V] =
    ZIO.fromEither(json.get[T](cursor).map(_.value))
      .mapError(InvalidRequest(s"Missing or invalid field $cursor.", _))
  
  def parseAttributeOpt[V, T <: Json {def value: V}](json: Json, cursor: JsonCursor[Json, T]) =
    ZIO.fromEither(json.get[T](cursor).map(_.value)).option
  
  def getJsonBody(r: Response): Task[Json] = {
    for
      body <- r.body.asString
      json <- ZIO.fromEither(body.fromJson[Json]).orElseSucceed(Json.Obj())
    yield json
  }
  
}
