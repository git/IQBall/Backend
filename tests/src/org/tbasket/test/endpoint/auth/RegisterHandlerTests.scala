package org.tbasket.test.endpoint.auth

import org.tbasket.endpoint.Endpoint.handle
import org.tbasket.test.TestUtils
import org.tbasket.test.TestUtils.*
import org.tbasket.test.endpoint.TBasketPageSpec
import org.tbasket.test.endpoint.auth.RegisterHandlerTests.test
import zio.*
import zio.http.*
import zio.http.model.{HeaderNames, Headers, Status}
import zio.json.ast.JsonCursor
import zio.test.*
import zio.test.Assertion.*

object RegisterHandlerTests extends TBasketPageSpec("/auth/register") {
  
  
  private def requestsSpec = suite("bad request tests")(
    ZIO.attempt(Map( //TODO test all wrong combinations
      "empty packet" -> Body.empty,
      "with no mail attribute" -> Body.fromString("""{"password": "123445678"}"""),
      "with no password attribute" -> Body.fromString("""{"email":"valid.mail@x.y"}"""),
      "with invalid form data" -> Body.fromString("""{this is a corrupted form data}""")
    )).map(_.map { case (name, body) =>
      test(name) {
        for
          response <- handle(Request.post(body, url))
          json <- getJsonBody(response)
          errorType <- parseAttribute(json, JsonCursor.field("error").isString)
        yield
          assert(response)(hasField("status", _.status, equalTo(Status.BadRequest)))
            && assertTrue(errorType.startsWith("invalid"))
      }
    })
  )
  
  private def registerSpec = suite("register tests")(
    test("register then try register again") {
      (for
        resp <- handle(Request.post(Body.fromString("""{"name":"tuaillon","forename":"leo","email":"leo.tuaillon@etu.uca.fr","password":"bouhours"}"""), url))
      yield
        assert(resp)(hasField("status", _.status, equalTo(Status.Found)))
          && assert(resp)(hasField("body", _.body, equalTo(Body.empty))) //TODO assert that the cookie name is JWT
          && assert(resp)(hasField("headers", _.headers, exists(hasField("key", _.key, equalTo(HeaderNames.setCookie)))))
          && assert(resp)(hasField("headers", _.headers, contains(Headers.location("/"))))
        )
        *>
        (for
          resp <- handle(Request.post(Body.fromString("""{"name":"tuaillon","forename":"leo","email":"leo.tuaillon@etu.uca.fr","password":"bouhours"}"""), url))
          json <- getJsonBody(resp)
          errorType <- parseAttributeOpt(json, JsonCursor.field("error").isString)
        
        yield
          assert(resp)(hasField("status", _.status, equalTo(Status.NotAcceptable)))
            && assert(errorType)(isSome(equalTo("already registered")))
            && assert(resp)(hasField("headers", _.headers, contains(Headers.location("/login")))))
    },
    test("register bad email") {
      for
        resp <- handle(Request.post(Body.fromString("""{"name":"tuaillon","forename":"leo","email":"leo.tuaillonbadmail","password":"bouhours"}"""), url))
        json <- getJsonBody(resp)
        errorType <- parseAttributeOpt(json, JsonCursor.field("error").isString)
      yield
        assert(resp)(hasField("status", _.status, equalTo(Status.ExpectationFailed)))
          && assert(errorType)(isSome(equalTo("invalid email")))
    },
    test("register bad password") {
      for
        resp <- handle(Request.post(Body.fromString("""{"name":"tuaillon","forename":"leo","email":"leo.tuaillon@etu.uca.fr","password":"1234"}"""), url))
        json <- getJsonBody(resp)
        errorType <- parseAttributeOpt(json, JsonCursor.field("error").isString)
      yield
        assert(resp)(hasField("status", _.status, equalTo(Status.ExpectationFailed)))
          && assert(errorType)(isSome(equalTo("invalid password")))
    },
    test("register bad name") {
      for
        resp <- handle(Request.post(Body.fromString("""{"name":"","forename":"leo","email":"leo.tuaillon@etu.uca.fr","password":"123456"}"""), url))
        json <- getJsonBody(resp)
        errorType <- parseAttributeOpt(json, JsonCursor.field("error").isString)
      yield
        assert(resp)(hasField("status", _.status, equalTo(Status.ExpectationFailed)))
          && assert(errorType)(isSome(equalTo("invalid name")))
    },
    test("register bad forename") {
      for
        resp <- handle(Request.post(Body.fromString("""{"name":"tuaillon","forename":"","email":"leo.tuaillon@etu.uca.fr","password":"bouhours"}"""), url))
        json <- getJsonBody(resp)
        errorType <- parseAttributeOpt(json, JsonCursor.field("error").isString)
      yield
        assert(resp)(hasField("status", _.status, equalTo(Status.ExpectationFailed)))
          && assert(errorType)(isSome(equalTo("invalid forename")))
    }
  )
  
  override def tspec = suite("/register page handler")(
    requestsSpec,
    registerSpec
  )
}
