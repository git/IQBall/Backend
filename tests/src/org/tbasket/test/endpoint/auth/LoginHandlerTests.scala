package org.tbasket.test.endpoint.auth

import io.getquill.jdbczio.Quill
import io.getquill.{SnakeCase, SqliteZioJdbcContext}
import org.tbasket.auth.Authenticator
import org.tbasket.data.{Database, DatabaseContext}
import org.tbasket.endpoint.Endpoint
import org.tbasket.endpoint.Endpoint.handle
import org.tbasket.endpoint.auth.LoginHandler
import org.tbasket.error.*
import org.tbasket.test.TestUtils.*
import org.tbasket.test.endpoint.TBasketPageSpec
import org.tbasket.test.{TestLayers, TestUtils}
import zio.*
import zio.http.*
import zio.http.model.Headers.{Header, empty}
import zio.http.model.{HeaderNames, Headers, Status}
import zio.http.netty.client.ConnectionPool
import zio.json.*
import zio.json.ast.{Json, JsonCursor}
import zio.test.*
import zio.test.Assertion.*

object LoginHandlerTests extends TBasketPageSpec("/auth/login") {
  
  
  private def requestsSpec = suite("bad request tests")(
    ZIO.attempt(Map(
      "empty packet" -> Body.empty,
      "with no mail attribute" -> Body.fromString("""{"password":"bouhours"}"""),
      "with no password attribute" -> Body.fromString("""{"email":"valid.email@not.very"}"""),
      "with invalid form data" -> Body.fromString("""{this is a corrupted form data}""")
    )).map(_.map { case (name, body) =>
      test(name) {
        for
          response <- handle(Request.post(body, url))
          json <- getJsonBody(response)
          errorType <- parseAttribute(json, JsonCursor.field("error").isString)
        yield
          assert(response)(hasField("status", _.status, equalTo(Status.BadRequest)))
            && assertTrue(errorType == "invalid request")
      }
    })
  )
  
  private def loginSpec = {
    suite("login situation tests")(
      test("login with unknown account") {
        for
          response <- handle(Request.post(Body.fromString("""{"password":"bouhours","email":"unknownaccount@gmail.com"}"""), url))
          json <- getJsonBody(response)
          errorType <- parseAttribute(json, JsonCursor.field("error").isString)
        yield
          //assert that the response error is of type unauthorized and headers are Location: /register
          assert(response)(hasField("status", _.status, equalTo(Status.Found)))
            && assert(errorType)(equalTo("unauthorized"))
            && assert(response)(hasField("headers", _.headers, contains(Headers.location("/register"))))
      },
      
      test("login with known account") {
        for
          response <- handle(Request.post(Body.fromString("""{"password":"123456","email":"maximebatista18@gmail.com"}"""), url))
        yield
          assert(response)(hasField("status", _.status, equalTo(Status.Found)))
            && assert(response)(hasField("body", _.body, equalTo(Body.empty)))
            && assert(response)(hasField("headers", _.headers, exists(hasField("key", _.key, equalTo(HeaderNames.setCookie)))))
      },
      
      test("login with known account wrong password") {
        for
          fiber <- handle(Request.post(Body.fromString("""{"password":"wrong","email":"maximebatista18@gmail.com"}"""), url)).fork
          _ <- TestClock.adjust(1.seconds)
          response <- ZIO.fromFiber(fiber)
          json <- getJsonBody(response)
          errorType <- parseAttribute(json, JsonCursor.field("error").isString)
        yield
          assert(response)(hasField("status", _.status, equalTo(Status.Unauthorized)))
            && assert(errorType)(equalTo("unauthorized"))
      }
    )
  }
  
  override def tspec = suite("/login page handler")(
    requestsSpec,
    loginSpec
  )
}
