package org.tbasket.test.endpoint

import io.getquill.NamingStrategy
import io.getquill.context.qzio.ZioJdbcContext
import io.getquill.context.sql.idiom.SqlIdiom
import org.tbasket.auth.Authenticator
import org.tbasket.data.DatabaseContext
import org.tbasket.endpoint.auth.LoginHandler.login
import org.tbasket.test.TestLayers.*
import zio.*
import zio.http.netty.client.ConnectionPool
import zio.http.{Client, ClientConfig, URL}
import zio.test.{Spec, TestEnvironment, ZIOSpecDefault}

import javax.sql.DataSource

abstract class TBasketPageSpec(location: String) extends ZIOSpecDefault {
  
  protected val url = URL.fromString(s"http://localhost$location") match
    case Left(exception) => throw exception
    case Right(url)      => url
  
  protected def tspec: Spec[
    DataSource & ClientConfig & Authenticator & ConnectionPool &
    Scope & DatabaseContext & Client, Any
  ]
  
  final override def spec = tspec.provide(
    db.datasourceLayer,
    db.contextLayer,
    auth,
    ConnectionPool.fixed(1),
    Scope.default,
    ClientConfig.default,
    Client.live)
}
