package org.tbasket.test

import com.sun.nio.file.ExtendedOpenOption
import io.netty.buffer.ByteBuf

import java.nio.channels.Pipe
import java.nio.file.{Files, Path, StandardOpenOption}

object TestEmitter:
  val PORT = 5457
  
  def start(): Unit = {
    println("SETTING UP LOCAL JWT EMITTER FOR TESTS")
    val emitterPresence = Path.of("/tmp/emitter.presence")
    Files.deleteIfExists(emitterPresence)
    
    val process = new ProcessBuilder(
      "bash",
      "./mill", "--disable-ticker", "JWTEmitter.run",
      "-k", "/tmp/keys/key.pcqks",
      "-p", TestServerConfig.emitterURL.port.get.toString
    )
      .inheritIO()
      .start()
    Runtime.getRuntime.addShutdownHook(new Thread((() => process.destroy()): Runnable))
    
    //the emitter will create the /tmp/emitter.presence file once it started, this is to inform us that the server is OP
    while (Files.notExists(emitterPresence))
      Thread.sleep(500)
    if (Files.readString(emitterPresence) == "A") {
      System.err.println("Emitter did not start successfully")
      System.exit(1)
    }
    Files.delete(emitterPresence)
    println("EMITTER PRESENCE DETECTED AND INITIALIZED, CONTINUING...")
  }
