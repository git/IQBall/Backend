package org.tbasket.test

import org.tbasket.config.ServerConfig
import pdi.jwt.JwtAlgorithm
import zio.http.URL
import TestEmitter.PORT
import pdi.jwt.algorithms.JwtAsymmetricAlgorithm

import java.nio.file.{Files, Path}
import java.security.cert.{Certificate, CertificateFactory}
import scala.reflect.io

object TestServerConfig extends ServerConfig {
  new ProcessBuilder("bash", "./tests/resources/generate_keys.sh")
    .inheritIO()
    .start()
    .waitFor()
  
  private final val CertFactory = CertificateFactory.getInstance("X509")
  
  override def emitterURL: URL = URL.fromString(s"http://localhost:$PORT").getOrElse(null)
  
  override def emitterCertificate: Certificate =
    CertFactory.generateCertificate(Files.newInputStream(Path.of("/tmp/keys/public.cert")))
  
  override def emitterKeyAlgorithm: JwtAsymmetricAlgorithm = JwtAlgorithm.RS256
  
  override def endpointPort: Int = 5454
  
  override def databaseConfigName: String = "test-database"
  
  override def pagesLocation: Option[Path] = None
}
