#!/usr/bin/bash

echo GENERATING TEMPORARY KEY PAIRS FOR TESTS

rm -r /tmp/keys &> /dev/null
mkdir -p /tmp/keys
cd /tmp/keys
keytool -genkeypair \
  -alias key \
  -keyalg RSA \
  -keysize 4069 \
  -sigalg SHA256withRSA \
  -storetype PKCS12 \
  -keystore store.p12 \
  -dname "CN=x.y.com, OU=TB, O=TBA, L=dzqdz, S=dqzdzq, C=GB" \
  -storepass 123456789 \
  -keypass 123456789

keytool -v -export -file public.cert -keystore store.p12 -alias key -storepass 123456789

openssl pkcs12 -in store.p12 -nodes -nocerts -out key.pem -passin pass:123456789
openssl pkcs8 -topk8 -inform PEM -outform DER -in key.pem -out key.pcqks -nocrypt

#openssl pkcs8 -topk8 -inform PEM -outform DER -in test.keystore -out private.pcks -passin pass:123456789
#openssl pkcs8 -topk8 -inform PEM -outform DER -in test.keystore -out public.cert -passin pass:123456789
