package org.tbasket.jwt

import org.slf4j.LoggerFactory
import pdi.jwt.JwtAlgorithm
import pdi.jwt.algorithms.JwtUnknownAlgorithm
import zio.*
import zio.http.*
import zio.http.ServerConfig.LeakDetectionLevel
import zio.http.model.{Method, Status}
import zio.stream.*

import java.nio.file.{Files, Path}
import java.security.{KeyFactory, PrivateKey}
import java.security.spec.{KeySpec, PKCS8EncodedKeySpec, RSAPrivateKeySpec, X509EncodedKeySpec}
import java.time.Duration
import scala.util.chaining.scalaUtilChainingOps

object Main extends ZIOAppDefault:

  private val KeyFactory = java.security.KeyFactory.getInstance("RSA")
  private val EmitterPresenceHook = Path.of("/tmp/emitter.presence")
  
  private val app = Http.collectZIO[Request] {
    case r@(Method.GET | Method.POST) -> _  =>
      ZIO.serviceWithZIO[JwtGenerator](_.generateTokenResponse(r))
    case _                         =>
      ZIO.succeed(Response(status = Status.MethodNotAllowed))
  }
  
  private def parsePort(port: Option[String]): Task[Int] =
    port match
      case None => ZIO.dieMessage("Must provide the port argument")
      case Some(port) => port.toIntOption match
          case Some(port) if port > 0 && port < 65536 => ZIO.succeed(port)
          case Some(oorPort) => ZIO.dieMessage(s"'$oorPort' is out of range.'")
          case None => ZIO.dieMessage("given argument is not a valid integer")

  private def loadKey(keyFile: Option[String]): Task[PrivateKey] =
    keyFile match
      case None => ZIO.dieMessage("Key RSA File not given")
      case Some(file) =>
        ZIO
          .attempt(Path.of(file))
          .mapAttempt(Files.readAllBytes)
          .mapAttempt(new PKCS8EncodedKeySpec(_))
          .mapAttempt(KeyFactory.generatePrivate)

  private def parseArgs(args: Chunk[String]): Task[(Int, PrivateKey)] =
    def groups = args.toList.grouped(2)
    val keyFile = groups.collectFirst {
      case ("-k" | "--key") :: value :: Nil => value
    }
    val port = groups.collectFirst {
      case ("-p" | "--port") :: value :: Nil => value
    }
    parsePort(port) <&> loadKey(keyFile)

  private def onStart(port: Int) =
    Console.printLine(s"JWT AppToken open on port $port") *> ZIO.attempt {
      Files.deleteIfExists(EmitterPresenceHook)
      Files.createFile(EmitterPresenceHook)
    }

  private def startServer(port: Int, key: PrivateKey) =
    val config = ServerConfig.default
      .port(port)
      .leakDetection(LeakDetectionLevel.PARANOID)

    val generator =
      new JwtGenerator(Duration.ofDays(15), key, JwtAlgorithm.RS256)
    val configLayer = ServerConfig.live(config)
    (Server.install(app) *> onStart(port) *> ZIO.never)
      .provide(configLayer, Server.live, ZLayer.succeed(generator))
      .catchAllCause(c => {
        Files.writeString(EmitterPresenceHook, "A")
        ZIO.failCause(c)
      })

  val run =
    ZIO.serviceWithZIO[ZIOAppArgs](args => parseArgs(args.getArgs))
      .flatMap(startServer)
