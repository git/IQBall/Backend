import mill._, scalalib._, scalafmt._

trait ServerModule extends ScalaModule with ScalafmtModule {
  
  //override def scalacOptions = Seq("-explain")
  
  override final def scalaVersion = "3.2.0"
  
  override def ivyDeps = Agg(
    ivy"dev.zio::zio:2.0.6",
    ivy"org.apache.logging.log4j:log4j-slf4j-impl:2.19.0",
    ivy"org.apache.logging.log4j:log4j-core:2.19.0",
    ivy"org.apache.logging.log4j:log4j-api:2.19.0",
  )
}

trait HttpModule extends ServerModule {
  
  
  
  override def ivyDeps = super.ivyDeps() ++ Agg(
    ivy"dev.zio::zio-http:0.0.3",
    ivy"dev.zio::zio-streams:2.0.6",
    

    ivy"com.github.jwt-scala::jwt-zio-json:9.1.2"
  )
}

/**
 * Simple module whose only job is to generate JWT Tokens
 * */
object JWTEmitter extends HttpModule

/**
 * Business layer of a server
 * */
object Core extends HttpModule { //also handles http
  override def ivyDeps = super.ivyDeps() ++ Agg(
    ivy"io.getquill::quill-jdbc-zio:4.6.0",
    ivy"org.xerial:sqlite-jdbc:3.40.0.0",
  
    ivy"io.circe::circe-core:0.14.3",
    ivy"io.circe::circe-parser:0.14.3",
    ivy"io.circe::circe-generic:0.14.3",
  )
  
}


object tests extends TestModule with ServerModule {
  override def ivyDeps = Agg(
    ivy"dev.zio::zio-test:2.0.6",
    ivy"dev.zio::zio-test-sbt:2.0.6",
    ivy"dev.zio::zio-test-magnolia:2.0.6",
  )
  
  override def testFramework = "zio.test.sbt.ZTestFramework"
  
  
  override def finalMainClass = "org.tbasket.test.pages.LoginPageHandlerTests"
  
  override def moduleDeps = Seq(Core, JWTEmitter)
}