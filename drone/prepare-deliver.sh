#!/usr/bin/env bash

mkdir /root/.ssh
echo "$SSH_PRIVATE_KEY" > /root/.ssh/id_rsa
chmod 0600 /root/.ssh/*
chmod 700 /root/.ssh
chmod 700 /root

case "$DRONE_BRANCH" in
  "production")
    export USER=mabatista1
    export IP=lisbonne.iut-clermont.uca.fr
    export DIR=tbasket
  ;;
  "")
    echo '$DRONE_BRANCH is missing' >&2
    exit 1
  ;;
  *)
    export USER=tbasket-dev
    export IP=92.132.64.175
    export DIR=/server/TBasket
esac