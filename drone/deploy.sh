#!/usr/bin/env bash

DIR=$(readlink -e "$(dirname "$0")")
PROD_SERVER_JAR_NAME="server-prod.jar"
NEW_SERVER_JAR_NAME="out.jar"

PROD_EMITTER_JAR_NAME="JWTEmitter-prod.jar"
NEW_EMITTER_JAR_NAME="JWTEmitter.jar"

find_pid() {
  ps -aux | tr -s " " | grep -E "\bjava -jar .*$1\b" | cut -d " " -f2
}

SERVER_PROD_PID=$(find_pid $PROD_SERVER_JAR_NAME)
EMITTER_PROD_PID=$(find_pid $PROD_EMITTER_JAR_NAME)

try_shutdown() {
  if [ "$1" ]; then
    #will cause the old server to gracefully shutdown
    echo "shutting down old $2 version ..."
    kill "$PROD_PID"
    while [ ! "$(find_pid $2)" ]; do sleep 1; done #sleep until process ends
    echo "$2 shut down"
  fi
}

try_shutdown "$SERVER_PROD_PID" "$PROD_SERVER_JAR_NAME"
try_shutdown "$EMITTER_PROD_PID" "$PROD_EMITTER_JAR_NAME"


rm "$DIR/$PROD_SERVER_JAR_NAME"
mv "$DIR/$NEW_SERVER_JAR_NAME" "$DIR/$PROD_SERVER_JAR_NAME" || ls
mv "$DIR/$NEW_EMITTER_JAR_NAME" "$DIR/$PROD_EMITTER_JAR_NAME" || ls

SCREEN="basket"

# create screen if not exists
if ! screen -ls | grep -q -E "\b[0-9]+\.$SCREEN\b"; then
  screen -S "$SCREEN" -d -m
fi

chmod 755 /run/screen
screen -d -r "$SCREEN" -X stuff $"$DIR/start.sh\n"
echo "server is started into $SCREEN screen."
