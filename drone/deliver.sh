#!/usr/bin/env bash

chmod 700 ./drone/prepare-deliver.sh
source ./drone/prepare-deliver.sh


echo "making delivery onto '$USER@$IP:$DIR/backend'"
mv "out/JWTEmitter/assembly.dest/out.jar" "out/JWTEmitter/assembly.dest/JWTEmitter.jar"
scp -o "StrictHostKeyChecking no" "out/Core/assembly.dest/out.jar" "out/JWTEmitter/assembly.dest/JWTEmitter.jar" "drone/deploy.sh" "drone/start.sh" "$USER@$IP:$DIR/backend"
echo "chmod 700 $DIR/backend/*; cd $DIR/backend; ./deploy.sh" | ssh -o "StrictHostKeyChecking no" "$USER@$IP"
